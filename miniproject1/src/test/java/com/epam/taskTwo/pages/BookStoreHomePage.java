package com.epam.taskTwo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookStoreHomePage {

    Map<String,ArrayList<String>> bookDetails = new HashMap<>();
    protected WebDriver driver;
    public BookStoreHomePage(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }
    @FindAll(@FindBy(xpath = "//div[@class=\"rt-tr-group\"]/div/div/div/span/a"))
    private List<WebElement> titles;

    @FindAll(@FindBy(xpath = "//div[@class=\"rt-tr-group\"]/div/div/div/parent::div/following-sibling::div"))
    private List<WebElement> authorPublishers;

    public Map<String, ArrayList<String>> getBookDetails() {
        for(int i=0,j=0;i<titles.size();i++,j=j+2)
        {
            ArrayList<String> details = new ArrayList<>();
            details.add(authorPublishers.get(j).getText());
            details.add(authorPublishers.get(j+1).getText());
            bookDetails.put(titles.get(i).getText(),details);
        }
        return bookDetails;
    }
}
