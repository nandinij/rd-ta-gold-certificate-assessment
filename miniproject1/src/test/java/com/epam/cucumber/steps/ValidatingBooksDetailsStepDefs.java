package com.epam.cucumber.steps;

import com.epam.reportportal.annotations.TemplateConfig;
import com.epam.taskTwo.pages.BookStoreHomePage;
import com.epam.webdriver.DriverSingleton;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import org.etsi.uri.x01903.v13.ResponderIDType;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.*;

import static io.restassured.RestAssured.given;

public class ValidatingBooksDetailsStepDefs {
    Response response;
    WebDriver driver= DriverSingleton.getDriver();
    Map<String, ArrayList<String>> bookDetails= new HashMap<>();
    @Given("user loads the get url")
    public void userLoadsTheGetUrl() {
        RestAssured.baseURI="https://demoqa.com/BookStore/v1/Books";
    }

    @When("user sends the get request")
    public void userSendsTheGetRequest() {
       response= given()
               .request(Method.GET);
    }

    @Then("books details are stored into a map")
    public Map<String, ArrayList<String>> booksDetailsAreStoredIntoAMap() {
        List list = response.jsonPath().getList("books");

        for(Object books: list)
        {
            Map newMap=(Map) books;
            ArrayList<String> list1 = new ArrayList<>();
            list1.add((String) newMap.get("author"));
            list1.add((String) newMap.get("publisher"));
            bookDetails.put((String) newMap.get("title"),list1);

        }
        return bookDetails;
    }

        //{Learning JavaScript Design Patterns=[Addy Osmani, O'Reilly Media], Programming JavaScript Applications=[Eric Elliott, O'Reilly Media], Speaking JavaScript=[Axel Rauschmayer, O'Reilly Media], Designing Evolvable Web APIs with ASP.NET=[Glenn Block et al., O'Reilly Media], Git Pocket Guide=[Richard E. Silverman, O'Reilly Media], You Don't Know JS=[Kyle Simpson, O'Reilly Media], Eloquent JavaScript, Second Edition=[Marijn Haverbeke, No Starch Press], Understanding ECMAScript 6=[Nicholas C. Zakas, No Starch Press]}
      //  {Learning JavaScript Design Patterns=[Addy Osmani, O'Reilly Media], Programming JavaScript Applications=[Eric Elliott, O'Reilly Media], Speaking JavaScript=[Axel Rauschmayer, O'Reilly Media], Designing Evolvable Web APIs with ASP.NET=[Glenn Block et al., O'Reilly Media], Git Pocket Guide=[Richard E. Silverman, O'Reilly Media], You Don't Know JS=[Kyle Simpson, O'Reilly Media], Eloquent JavaScript, Second Edition=[Marijn Haverbeke, No Starch Press], Understanding ECMAScript 6=[Nicholas C. Zakas, No Starch Press]}

    BookStoreHomePage bookStoreHomePage = new BookStoreHomePage(driver);

    @Given("user loads the books page url")
    public void userLoadsTheBooksPageUrl() {
        driver.get("https://demoqa.com/books");
    }


//    @Then("validating books with api response")
//    public void validatingBooksWithApiResponse() {
//        Map<String,ArrayList<String>> actualResult = bookStoreHomePage.getBookDetails();
//
//        System.out.println(bookDetails.toString());
//        Assert.assertEquals(actualResult.toString(),bookDetails.toString());
//}

    @Then("validating books with api response {string}")
    public void validatingBooksWithApiResponse(String expectedResult) {
        Map<String,ArrayList<String>> actualResult = bookStoreHomePage.getBookDetails();

        Assert.assertEquals(actualResult.toString(),expectedResult);
    }
}


