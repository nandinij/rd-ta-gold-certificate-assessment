package com.epam.cucumber.steps;

import com.epam.pages.AbstractPage;
import com.epam.pages.DashBoardPage;
import com.epam.pages.LoginPage;
import com.epam.taskTwo.pages.BookStoreHomePage;
import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;

public class PicoContainer {
    protected MutablePicoContainer container;
    public PicoContainer()
    {
        container=new DefaultPicoContainer();
        container.addComponent(DashBoardPage.class);
        container.addComponent(LoginPage.class);
        container.addComponent(BookStoreHomePage.class);
    }
    public MutablePicoContainer getContainer()
    {
        return container;
    }
}
