package com.epam.cucumber.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.log4testng.Logger;


import static io.restassured.RestAssured.given;

public class APILoginValidationStepDefs {

    public static final Logger LOGGER = Logger.getLogger(APILoginValidationStepDefs.class);
    Response response;

    @Given("user loads the base url")
    public void userLoadsTheBaseUrl() {
        RestAssured.baseURI = "https://demoqa.com/Account/v1/User";
        LOGGER.info("Base url is loaded");
    }

    @When("user passes user name and password")
    public void userPassesAndAsPassword() {
        String userCredentials = "{\n" +
                "    \"userName\": \"balu\",\n" +
                "    \"password\": \"J@nandu983\"" +
                "}";
        response = given()
                .contentType("application/json")
                .body(userCredentials)
                .request(Method.POST);
        LOGGER.info("Post request is sent");

    }

    @Then("status code of {int} should be displayed")
    public void statusCodeOfShouldBeDisplayed(int expectedStatusCode) {
        response.prettyPrint();
        Assert.assertEquals(response.getStatusCode(), expectedStatusCode);
        LOGGER.info("status code is verified");
    }

    @And("response body should contain username {string}")
    public void responseBodyShouldContainUsername(String expectedUserName) {
        JsonPath jsonPath = response.jsonPath();
        Assert.assertEquals(jsonPath.get("username"), expectedUserName);
        LOGGER.info("response body is verified");
    }


}
