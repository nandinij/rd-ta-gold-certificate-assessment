package com.epam.cucumber.steps;

import com.epam.pages.DashBoardPage;
import com.epam.pages.LoginPage;
import com.epam.webdriver.DriverSingleton;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;


public class UILoginValidationStepDef {
    WebDriver driver = DriverSingleton.getDriver();
    LoginPage loginPage= new LoginPage(driver);
    DashBoardPage dashBoardPage = new DashBoardPage(driver);
    @Given("user loads the page url")
    public void userLoadsThePageUrl() {
        driver.get("https://demoqa.com/login");
    }

   @When("user enter username and password")
   public void userEntersUsernamePassword()
   {
       loginPage.enterUserName("nandini").enterPassword("J@nandu99483");
   }

    @And("clicks on login button")
    public void clicksOnLoginButton() {
        loginPage.clickOnLogin();
    }

    @Then("user should be navigated to Dashboard Page")
    public void userShouldBeNavigatedToDashboardPage() {
        Assert.assertEquals(driver.getCurrentUrl(),"https://demoqa.com/profile");
    }

    @Then("{string} should be displayed in Dashboard Page")
    public void shouldBeDisplayedInDashboardPage(String username) {

        Assert.assertEquals(username,dashBoardPage.getUserName());
    }
}
