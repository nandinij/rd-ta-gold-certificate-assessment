package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashBoardPage extends AbstractPage{
    protected WebDriver driver;
    public DashBoardPage(WebDriver driver) {
        super(driver);
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }
    @FindBy(id="userName-value")
    private WebElement userName;

    public String getUserName()
    {
       return getTextAfterElementVisibility(userName);
    }

}
