package com.epam.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverManager implements DriverManager{
    @Override
    public WebDriver initializeDriver() {
       WebDriver driver=new ChromeDriver();
       return driver;
    }
}
