package com.epam.webdriver;

import com.epam.exceptions.BrowserException;
import com.epam.utility.FileReader;

import static com.epam.constants.SeleniumConstants.BROWSER_PATH;


public class DriverFactory {
    public DriverManager getDriverManager()
    {
        switch (FileReader.readPropertiesFile(BROWSER_PATH).getProperty("browser")){
            case "CHROME" :
                return new ChromeDriverManager();
            case "EDGE" :
                return  new EdgeDriverManager();
            case "FIREFOX" :
                return new FirefoxDriverManager();
            default: throw new BrowserException("launch the correct browser");
        }
    }
}
