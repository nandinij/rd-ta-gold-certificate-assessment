package com.epam.webdriver;

import org.openqa.selenium.WebDriver;

public class DriverSingleton {
    public static WebDriver WEB_DRIVER_SINGLETON_INSTANCE;

    private DriverSingleton() {
        DriverFactory driverFactory = new DriverFactory();
        DriverManager driverManager = driverFactory.getDriverManager();
        WEB_DRIVER_SINGLETON_INSTANCE = driverManager.initializeDriver();
    }

    public static WebDriver getDriver() {
        if (WEB_DRIVER_SINGLETON_INSTANCE == null) {
            DriverSingleton driverSingleton = new DriverSingleton();
        }
        return WEB_DRIVER_SINGLETON_INSTANCE;
    }
}
