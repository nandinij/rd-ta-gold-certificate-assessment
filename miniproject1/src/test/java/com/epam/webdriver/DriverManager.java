package com.epam.webdriver;

import org.openqa.selenium.WebDriver;

public interface DriverManager {
    public WebDriver initializeDriver();
}
