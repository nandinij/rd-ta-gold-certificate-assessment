package com.epam.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class EdgeDriverManager implements DriverManager {
    @Override
    public WebDriver initializeDriver() {
        WebDriver driver = new EdgeDriver();
        return driver;
    }
}
