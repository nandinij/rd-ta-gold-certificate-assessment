package com.epam.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverManager implements DriverManager{
    @Override
    public WebDriver initializeDriver() {
        WebDriver driver= new FirefoxDriver();
        return driver;
    }
}
