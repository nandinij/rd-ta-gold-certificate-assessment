Feature: Validating books details obtained from API in UI

  Scenario: Getting the books details using API
    Given user loads the get url
    When user sends the get request
    Then books details are stored into a map

  Scenario: validating above response with ui
    Given user loads the books page url
    Then validating books with api response "{Learning JavaScript Design Patterns=[Addy Osmani, O'Reilly Media], Programming JavaScript Applications=[Eric Elliott, O'Reilly Media], Speaking JavaScript=[Axel Rauschmayer, O'Reilly Media], Designing Evolvable Web APIs with ASP.NET=[Glenn Block et al., O'Reilly Media], Git Pocket Guide=[Richard E. Silverman, O'Reilly Media], You Don't Know JS=[Kyle Simpson, O'Reilly Media], Eloquent JavaScript, Second Edition=[Marijn Haverbeke, No Starch Press], Understanding ECMAScript 6=[Nicholas C. Zakas, No Starch Press]}"
