package com.epam.exceptions;

public class BrowserException extends RuntimeException {
    public BrowserException(String errorMessage) {
        super(errorMessage);
    }
}
