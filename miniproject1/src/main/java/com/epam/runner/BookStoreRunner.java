package com.epam.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}, glue = {"com.epam.cucumber.steps"}, features = {"src/test/resources/features/ValidateBooksDetails.feature"})
public class BookStoreRunner extends AbstractTestNGCucumberTests {
}
